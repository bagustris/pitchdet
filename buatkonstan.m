% buatkonstan.m: make constants

function konstan = buatkonstan(constantsIn)
	if nargin > 0
		konstan = constantsIn;
	else
		konstan = struct();
		konstan.fftWindow = 2^12;		%Data points
        %konstan.frekuensiSampling=fs;
    end
    
    % frekuensi, sinyal akan dizeropadd mjd 2x panjang
	konstan.frek = zeros(1,konstan.fftWindow);
	b = 1:(konstan.fftWindow+1);
	konstan.frek= (b-1)*(konstan.frekuensiSampling/2)/konstan.fftWindow;

    % buat konstanta yg dibutuhkan poliponik
	% CB filterbank
	konstan.cb = zeros(1,32);
    
	% CB filterbank memiliki nilai yang sama
	b = 1:32;
	konstan.cb = 229.0*(10.0.^(b/21.4)-1.0); % pembagian frekuensi
	
    	% Perhitungan frekuensi bank
	% Sinyal whitening berdasarkan paper Kalpuri 2006 hal 2, section 2.1
	% buat bandpass filterbank, 30 sinyal segi3
	konstan.Hb = zeros(konstan.fftWindow+1,length(konstan.cb)-2);
	for i=2:(length(konstan.cb)-1) % segitiga dari (i-1:i+1), pusat pita
		kk=find(konstan.frek >=konstan.cb(i-1),1,'first'); % batas bawah
		while (konstan.frek(kk) <= konstan.cb(i+1))	% loop sampai batas atas
			if konstan.frek(kk) <= konstan.cb(i)
				konstan.Hb(kk,i-1) = (1-abs(konstan.cb(i)-konstan.frek(kk))/(konstan.cb(i)-konstan.cb(i-1)));
			else	% turun
				konstan.Hb(kk,i-1) = (1-abs(konstan.cb(i)-konstan.frek(kk))/(konstan.cb(i+1)-konstan.cb(i)));
			end
			kk = kk+1;
        end
       % figure(2);
       % plot(konstan.Hb(kk,i-1));
    end
	

    konstan.f0indeks = find(konstan.frek <= 1000 & konstan.frek >=20);
    konstan.f0kand = konstan.frek(konstan.f0indeks);
    
    % Hitung frekuensi bins untuk kandidat f0
	konstan.harmonics = 20;
	halfBinWidth = ((konstan.frekuensiSampling/2)/konstan.fftWindow)/2;
	for i = 1:length(konstan.f0kand)
		binIndices = [];
		for h = 1:konstan.harmonics
			testi = find(konstan.frek > (konstan.f0kand(i)*h-halfBinWidth) & konstan.frek < (konstan.f0kand(i)*h+halfBinWidth));
			binIndices = [binIndices testi];
		end
		konstan.f0kandFrekBins(i).binIndices = binIndices;
	end
	
	konstan.alpha = 52.0; % didapat dari sect.3 utk frame 93ms
	konstan.beta = 320.0; % didapat dari sect.3 utk frame 93ms
	konstan.dee = 0.89;   % dari paper sect.3
	
end
