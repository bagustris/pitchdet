% main.m : main program to run Multipitch Estimation based on Klapuri '06 publication

clear all; close all; clc;

[test, fs] = audioread('pianoTrumpet.wav');    % ganti nama file disini
konstan = struct();
% keyboard;
konstan.frekuensiSampling = fs;
konstan.panjangSampelPitchDituju = 0.1;
konstan.fftWindow = 2^nextpow2(fs*konstan.panjangSampelPitchDituju);
konstan.aktualSampel = konstan.fftWindow/fs;
konstan = buatkonstan(konstan);
konstan.fh = figure('position',[10 10 1000 500]);

% plot
subplot(211); plot(test);
hold on;
konstan.lapisH = plot(1:konstan.fftWindow,test(1:konstan.fftWindow),'r');
konstan.frekVisualisasiIndeks = find(konstan.frek <=1000);
konstan.fftH = plot(konstan.frek(konstan.frekVisualisasiIndeks),zeros(1,length(konstan.frekVisualisasiIndeks)));

subplot(212);
konstan.whitenedH = plot(konstan.frek(konstan.frekVisualisasiIndeks),zeros(1,length(konstan.frekVisualisasiIndeks)));
hold on;
konstan.detectedH = plot(konstan.frek(konstan.frekVisualisasiIndeks),zeros(1,length(konstan.frekVisualisasiIndeks)),'r');

for i = 1:konstan.fftWindow/2:length(test)-konstan.fftWindow;
	konstan.epoch = i:i+konstan.fftWindow-1;
	F0sTerdetek = poliponik(test(konstan.epoch),konstan);
	printF0s = [];
	for j = 1:length(F0sTerdetek)
		printF0s = [printF0s ' F' num2str(j-1) ' ' num2str(F0sTerdetek(j))];
	end
	disp(printF0s);
end
