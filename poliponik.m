%% poliponik.m : polyponic pitch detection (section 2.4 dlm paper)

function pitches = poliponik(signalIn,konstan)

	% diwindow hann
	hannWindowed = signalIn.*hanning(length(signalIn));

	% panjang sinyal dijadikan dua kali dengan menambahkan zeroes
	if size(hannWindowed,1) > size(hannWindowed,2)
		hannWindowed = hannWindowed';
    end
    
	appended = [hannWindowed zeros(1,length(hannWindowed))];
	fftSignal = fft(appended);
	fftSignal= fftSignal./(length(fftSignal)/2+1);
	fftSignal(1) = fftSignal(1)/2;
	fftAmp = abs(fftSignal(1:(konstan.fftWindow+1))); % abaikan separo fft kedua
	whitened = whiten(fftAmp,konstan);
	
	% estiamasi poliponik dan cancellation frekuensi
	pitches = [];
	F0sTerdetek = 0;
	frekTerdetek = zeros(size(whitened,1),size(whitened,2));
	smax = 0;
	S = [];
	S(1) = 0;

	% Perulangan Loop ketika smax bertambah
	while S(length(S)) >=smax
		datasalience = salience(whitened,konstan);  %ambil dr fungsi salience dibawah
		[~, indeks] = max(datasalience);
		[whitened, frekTerdetek] = pitchDicancel(whitened,frekTerdetek,konstan,indeks);
	
		% estimasi smax (pers. 8)
        gamma=0.7;       % nilai gamma didapat dari paper hal. 4
		F0sTerdetek = F0sTerdetek+1;
		sumDetected = sum(frekTerdetek);
		S = [S sumDetected/(F0sTerdetek^gamma)];  
		if S(length(S)) > smax
			smax = S(length(S));
			pitches(F0sTerdetek ) = konstan.f0kand(indeks);
		end
	end

	% estimasi poliponik dan pitch
	set(konstan.lapisH ,'xdata',konstan.epoch,'ydata',hannWindowed);
	set(konstan.fftH,'ydata',fftAmp(konstan.frekVisualisasiIndeks));
	set(konstan.whitenedH,'ydata',whitened(konstan.frekVisualisasiIndeks));
	set(konstan.detectedH,'ydata',frekTerdetek(konstan.frekVisualisasiIndeks));
	drawnow();
end

%% fungsi salience --> datasalience (sect. 2.2, pers. 7)
function datasalience = salience(whitened,konstan)
	datasalience = zeros(1,length(konstan.f0kand));
	for i= 1:length(konstan.f0kand)
		findices = konstan.f0kandFrekBins(i).binIndices;
		j=1:length(findices);
		datasalience(i) = sum((konstan.frekuensiSampling*konstan.frek(findices(j))+konstan.alpha)./(j.*konstan.frekuensiSampling.*konstan.frek(findices(j))+konstan.beta).*whitened(findices(j)));
	end
end

%% fungsi PitchDicancel --> Whiteted, frekTerdetek (sect. 2.4 di paper)
function [whitened, frekTerdetek] = pitchDicancel(whitened,frekTerdetek,konstan,indeks)
	
	% kanselasi frekuensi
	binsUtkCancel = konstan.f0kandFrekBins(indeks).binIndices;
	for j = 1:length(binsUtkCancel)
		% cancel bins yang bersebelahan 
		for k =-1:1
			frekTerdetek(binsUtkCancel(j)+k) = frekTerdetek(binsUtkCancel(j)+k)+((konstan.frekuensiSampling*konstan.frek(binsUtkCancel(j)+k)+konstan.alpha)/(j*konstan.frekuensiSampling*konstan.frek(binsUtkCancel(j)+k)+konstan.beta))*whitened(binsUtkCancel(j)+k);
			kurangi = frekTerdetek(binsUtkCancel(j)+k)*konstan.dee;
			whitened(binsUtkCancel(j)+k)=max([0 whitened(binsUtkCancel(j)+k)-kurangi]);
		end
	end
end
