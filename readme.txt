- Multipitch estimation based on summing harmonics, paper by Ansi Klapuri, 2006

Sound data:
- pianoTrumpet.wav: musical instrument obtained from Labrosa, piano-G5 (784 Hz) and Trumpet-C4 (261 Hz), originally 11KHz but resampled to 44KHz (Link, http://labrosa.ee.columbia.edu/sounds/instruments/)

Main Program:
- main.m : main program to run
Function
- whiten.m : spectral whitening based on 2.1
- buatkonstan.m : make constants
- poliponik.m : pitch detection of polyponics (including whiten, pitchDicancel and salience function)
