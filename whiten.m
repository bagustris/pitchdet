% whiten.m : spectral Whitening based on Kalpuri 2006 pp. 2, sect 2.1

function whitened = whiten(dataIn,konstan)
		whitened = zeros(1,length(dataIn));
		
        % hitung standar deviasi dan koefisian kompresi bandwise dalam pita
        % frekuensi (pers. 2)
		stdb = zeros(1,size(konstan.Hb,2));
		gammab = zeros(1,size(konstan.Hb,2));
        	v=0.33;    % parameter spectral whitening didapat dari paper
		for i = 1:size(konstan.Hb,2)
			stdb(i) = sqrt(sum(konstan.Hb(:,i)'.*(dataIn.^2))/length(dataIn));
			gammab(i) = stdb(i)^(v-1);
        end
        
		% interpolasi gamma_b
		gamma = zeros(1,konstan.fftWindow+1);
        
        % Set koefisien kompresi utk sebelumnya dibawah pusat pita pertama
        % dan diatas sesudahnya
		gamma(1:find(konstan.frek >= konstan.cb(2),1,'first')) = gammab(1);
		gamma(find(konstan.frek >= konstan.cb(31),1,'first'):length(gamma)) = gammab(length(gammab));
		for i = 1:(length(gammab)-1)
			initF	= find(konstan.frek >= konstan.cb(i+1),1,'first');
			endF	= find(konstan.frek >= konstan.cb(i+2),1,'first');
			gamma(initF:endF) = linspace(gammab(i),gammab(i+1),endF-initF+1);
        end
        
		% Whitening sinyal
		whitened = gamma.*dataIn;
end
